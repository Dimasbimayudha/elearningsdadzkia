-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 21, 2023 at 11:36 AM
-- Server version: 10.4.28-MariaDB
-- PHP Version: 8.0.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `register`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`username`, `password`) VALUES
('admin10', '$2y$10$1aAJ8lnUQjbWdP3g0batNe4boP6G6l05nCf.bFOeB6uPksjhJx6IK'),
('admin2', '$2y$10$pze7OqmLPqA.rCVrCRn0h.VUB0f2SojWS4IVsVdztDrel4PW7pe1i'),
('admin', 'admin123');

-- --------------------------------------------------------

--
-- Table structure for table `dosen`
--

CREATE TABLE `dosen` (
  `kode` varchar(4) NOT NULL,
  `nama` text NOT NULL,
  `kata_sandi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `dosen`
--

INSERT INTO `dosen` (`kode`, `nama`, `kata_sandi`) VALUES
('', 'admin', 'admin123'),
('2002', 'admin', '$2y$10$wMgiws/mdbceCdTFNV3ymORwv/WvH40wmBP59FOpbi2jPDTBTNnva'),
('2022', 'adi', '$2y$10$EmyFw2DSTxFiQH3X1NC6HObGZuRjn22U1qOv6wNd8L9BaQi4YuSq6');

-- --------------------------------------------------------

--
-- Table structure for table `jawaban_ujian`
--

CREATE TABLE `jawaban_ujian` (
  `kode` varchar(8) NOT NULL,
  `ujian` varchar(8) NOT NULL,
  `mahasiswa` varchar(7) NOT NULL,
  `soal` varchar(8) NOT NULL,
  `jawaban` text NOT NULL,
  `poin` float DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `kehadiran`
--

CREATE TABLE `kehadiran` (
  `kode` int(8) NOT NULL,
  `pertemuan` varchar(8) NOT NULL,
  `mahasiswa` varchar(7) NOT NULL,
  `hadir` tinyint(1) NOT NULL DEFAULT 0,
  `keterangan` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `kelas`
--

CREATE TABLE `kelas` (
  `kode` varchar(8) NOT NULL,
  `mata_kuliah` varchar(5) NOT NULL,
  `nama` varchar(2) NOT NULL,
  `kapasitas` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `kelas`
--

INSERT INTO `kelas` (`kode`, `mata_kuliah`, `nama`, `kapasitas`) VALUES
('KLS30053', 'AB001', '7A', 20);

--
-- Triggers `kelas`
--
DELIMITER $$
CREATE TRIGGER `del_related_kontrak_kls` BEFORE DELETE ON `kelas` FOR EACH ROW BEGIN
    DELETE FROM Kontrak_Kelas WHERE kelas = OLD.kode;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `del_related_pertemuan` BEFORE DELETE ON `kelas` FOR EACH ROW BEGIN
    DELETE FROM Pertemuan WHERE kelas = OLD.kode;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `kontrak_kelas`
--

CREATE TABLE `kontrak_kelas` (
  `kode` varchar(8) NOT NULL,
  `mahasiswa` varchar(7) NOT NULL,
  `kelas` varchar(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `kontrak_kelas`
--

INSERT INTO `kontrak_kelas` (`kode`, `mahasiswa`, `kelas`) VALUES
('KKS64904', '1011', 'KLS30053');

-- --------------------------------------------------------

--
-- Table structure for table `kp`
--

CREATE TABLE `kp` (
  `name` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `pass` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `kp`
--

INSERT INTO `kp` (`name`, `email`, `pass`) VALUES
('admin', 'admin123', 'admin123');

-- --------------------------------------------------------

--
-- Table structure for table `mahasiswa`
--

CREATE TABLE `mahasiswa` (
  `nim` varchar(7) NOT NULL,
  `nama_lengkap` text NOT NULL,
  `kata_sandi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `mahasiswa`
--

INSERT INTO `mahasiswa` (`nim`, `nama_lengkap`, `kata_sandi`) VALUES
('1011', 'dimas', '$2y$10$b8zH5YrwOCBhmvO71ASp2O2h5dSUYvaB04UflEyJqzWxn2eSGx9v2');

--
-- Triggers `mahasiswa`
--
DELIMITER $$
CREATE TRIGGER `del_related_kontrak_mhs` BEFORE DELETE ON `mahasiswa` FOR EACH ROW BEGIN
    DELETE FROM Kontrak_Kelas WHERE mahasiswa = OLD.nim;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `mata_kuliah`
--

CREATE TABLE `mata_kuliah` (
  `kode` varchar(5) NOT NULL,
  `nama` text NOT NULL,
  `semester` int(11) NOT NULL,
  `sks` int(11) NOT NULL DEFAULT 1,
  `thn_mulai` year(4) NOT NULL,
  `thn_selesai` year(4) NOT NULL,
  `jml_pertemuan` int(11) NOT NULL DEFAULT 16,
  `dosen_pengampu1` varchar(4) NOT NULL,
  `dosen_pengampu2` varchar(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `mata_kuliah`
--

INSERT INTO `mata_kuliah` (`kode`, `nama`, `semester`, `sks`, `thn_mulai`, `thn_selesai`, `jml_pertemuan`, `dosen_pengampu1`, `dosen_pengampu2`) VALUES
('AB001', 'matematika', 7, 20, '2023', '2024', 3, '2002', NULL);

--
-- Triggers `mata_kuliah`
--
DELIMITER $$
CREATE TRIGGER `del_related_kelas` BEFORE DELETE ON `mata_kuliah` FOR EACH ROW BEGIN
    DELETE FROM Kelas WHERE mata_kuliah = OLD.kode;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `del_related_rps` BEFORE DELETE ON `mata_kuliah` FOR EACH ROW BEGIN
    DELETE FROM RPS WHERE mata_kuliah = OLD.kode;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `del_related_silabus` BEFORE DELETE ON `mata_kuliah` FOR EACH ROW BEGIN
    DELETE FROM Silabus WHERE mata_kuliah = OLD.kode;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `materi`
--

CREATE TABLE `materi` (
  `kode` varchar(8) NOT NULL,
  `pertemuan` varchar(8) NOT NULL,
  `judul` text NOT NULL,
  `deskripsi` text DEFAULT NULL,
  `nama_file` text DEFAULT NULL,
  `mimetype` text DEFAULT NULL,
  `url` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `materi`
--

INSERT INTO `materi` (`kode`, `pertemuan`, `judul`, `deskripsi`, `nama_file`, `mimetype`, `url`) VALUES
('MTR04023', 'AA123', 'Matematika', 'aa', 'MTR04023_WhatsApp Image 2023-11-10 at 20.31.43.jpeg', 'image/jpeg', NULL),
('MTR09192', 'AA123', 'Matematika', 'bb', 'MTR09192_WhatsApp Image 2023-11-10 at 20.31.43.jpeg', 'image/jpeg', NULL),
('MTR37565', 'AA123', 'Matematika', 'aa', 'MTR37565_WhatsApp Image 2023-11-10 at 20.31.43.jpeg', 'image/jpeg', NULL),
('MTR63850', 'AA123', 'Matematika', '', NULL, NULL, ''),
('MTR78191', '', 'Matematika', 'aa', 'MTR78191_WhatsApp Image 2023-11-10 at 20.31.43.jpeg', 'image/jpeg', NULL),
('MTR80246', 'AA123', 'IPA', 'asd', 'MTR80246_bab 1-3.docx', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', NULL),
('MTR80955', 'AA123', 'Matematika', 'aa', NULL, NULL, 'https://www.youtube.com/watch?v=HclExys9sz8'),
('MTR83186', 'AA123', 'Matematika', '', NULL, NULL, ''),
('MTR95316', 'AA123', 'Matematika', 'aa', 'MTR95316_WhatsApp Image 2023-11-10 at 20.31.43.jpeg', 'image/jpeg', NULL),
('MTR97330', 'AA123', 'Matematika', '', NULL, NULL, ''),
('MTR99484', 'AA123', 'Matematika', 'aa', 'MTR99484_WhatsApp Image 2023-11-10 at 20.31.43.jpeg', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `nilai_ujian`
--

CREATE TABLE `nilai_ujian` (
  `kode` int(11) NOT NULL,
  `ujian` varchar(8) NOT NULL,
  `mahasiswa` varchar(7) NOT NULL,
  `nilai` float NOT NULL DEFAULT 0,
  `sudah_dinilai` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `opsi_pg`
--

CREATE TABLE `opsi_pg` (
  `kode` varchar(8) NOT NULL,
  `soal` varchar(8) NOT NULL,
  `opsi_benar` text NOT NULL,
  `opsi_salah1` text NOT NULL,
  `opsi_salah2` text DEFAULT NULL,
  `opsi_salah3` text DEFAULT NULL,
  `opsi_salah4` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `paket_soal`
--

CREATE TABLE `paket_soal` (
  `kode` varchar(8) NOT NULL,
  `ujian` varchar(8) NOT NULL,
  `soal` varchar(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Triggers `paket_soal`
--
DELIMITER $$
CREATE TRIGGER `del_related_soal` BEFORE DELETE ON `paket_soal` FOR EACH ROW BEGIN
    DELETE FROM Soal WHERE kode = OLD.soal;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `pertemuan`
--

CREATE TABLE `pertemuan` (
  `kode` varchar(8) NOT NULL,
  `kelas` varchar(8) NOT NULL,
  `nomor_pert` int(11) NOT NULL,
  `topik` text NOT NULL,
  `deskripsi` text DEFAULT NULL,
  `waktu_akses` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `pertemuan`
--

INSERT INTO `pertemuan` (`kode`, `kelas`, `nomor_pert`, `topik`, `deskripsi`, `waktu_akses`) VALUES
('PRT16228', 'KLS30053', 1, 'matematika diskrit', 'pekalian', '2023-08-11 18:00:00');

--
-- Triggers `pertemuan`
--
DELIMITER $$
CREATE TRIGGER `buat_presensi` AFTER INSERT ON `pertemuan` FOR EACH ROW BEGIN
    DECLARE finished INTEGER DEFAULT FALSE;
    DECLARE nim VARCHAR(7);
    DECLARE list_mhs CURSOR FOR SELECT mahasiswa FROM Kontrak_Kelas WHERE kelas = NEW.kelas;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET finished = TRUE;

    OPEN list_mhs;
    getMhs: LOOP
        FETCH list_mhs INTO nim;
        IF finished = TRUE THEN
            LEAVE getMhs;
        END IF;

        -- menambahkan data ke tabel kehadiran
        INSERT INTO Kehadiran(mahasiswa, pertemuan) VALUES (nim, NEW.kode);

    END LOOP getMhs;
    CLOSE list_mhs;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `del_related_kehadiran` BEFORE DELETE ON `pertemuan` FOR EACH ROW BEGIN
    DELETE FROM Kehadiran WHERE pertemuan = OLD.kode;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `del_related_materi` BEFORE DELETE ON `pertemuan` FOR EACH ROW BEGIN
    DELETE FROM Materi WHERE pertemuan = OLD.kode;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `del_related_tugas` BEFORE DELETE ON `pertemuan` FOR EACH ROW BEGIN
    DELETE FROM Tugas WHERE pertemuan = OLD.kode;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `del_related_ujian` BEFORE DELETE ON `pertemuan` FOR EACH ROW BEGIN
    DELETE FROM Ujian WHERE pertemuan = OLD.kode;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `root`
--

CREATE TABLE `root` (
  `id` int(11) NOT NULL,
  `password` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rps`
--

CREATE TABLE `rps` (
  `kode` varchar(8) NOT NULL,
  `mata_kuliah` varchar(5) NOT NULL,
  `nama_file` text NOT NULL,
  `mimetype` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `silabus`
--

CREATE TABLE `silabus` (
  `kode` varchar(8) NOT NULL,
  `mata_kuliah` varchar(5) NOT NULL,
  `nama_file` text NOT NULL,
  `mimetype` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `soal`
--

CREATE TABLE `soal` (
  `kode` varchar(8) NOT NULL,
  `pilihan_ganda` tinyint(1) NOT NULL DEFAULT 0,
  `pertanyaan` text NOT NULL,
  `poin_benar` float NOT NULL DEFAULT 1,
  `poin_salah` float NOT NULL DEFAULT 0,
  `nama_file` text DEFAULT NULL,
  `mimetype` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Triggers `soal`
--
DELIMITER $$
CREATE TRIGGER `del_related_opsi_pg` BEFORE DELETE ON `soal` FOR EACH ROW BEGIN
    DELETE FROM Opsi_PG WHERE kode = OLD.kode;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `submit_tugas`
--

CREATE TABLE `submit_tugas` (
  `kode` varchar(8) NOT NULL,
  `mahasiswa` varchar(7) NOT NULL,
  `tugas` varchar(8) NOT NULL,
  `file_tugas` text NOT NULL,
  `mimetype` text NOT NULL,
  `waktu_pengumpulan` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_guru`
--

CREATE TABLE `tb_guru` (
  `id_guru` varchar(255) NOT NULL,
  `nm_guru` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `level` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `tb_guru`
--

INSERT INTO `tb_guru` (`id_guru`, `nm_guru`, `username`, `level`, `password`) VALUES
('123', 'Adi', 'adi', 'guru', '$2y$10$u2dtodfHX8Lv00QzEOknxOELdalvaZlnXZmZQfY1zm/Gte2I4Upvy'),
('20002', 'Dimas Ilham Bimayudha', 'admin', 'guru', '$2y$10$khdJBspFSUDX38xOjq0tIutnHYsBYHcKWiyQiXt8W/PAushdNHvza');

-- --------------------------------------------------------

--
-- Table structure for table `tb_kelas`
--

CREATE TABLE `tb_kelas` (
  `kode_kelas` varchar(20) NOT NULL,
  `nm_kelas` varchar(20) NOT NULL,
  `pengajar` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `tb_kelas`
--

INSERT INTO `tb_kelas` (`kode_kelas`, `nm_kelas`, `pengajar`) VALUES
('', '', ''),
('AA111', '', ''),
('AA123', 'kelas 5', '123'),
('AA301', 'KelasV', '20002'),
('KL123', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tb_siswa`
--

CREATE TABLE `tb_siswa` (
  `id_siswa` int(11) NOT NULL,
  `nm_siswa` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `level` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `tb_siswa`
--

INSERT INTO `tb_siswa` (`id_siswa`, `nm_siswa`, `username`, `password`, `level`) VALUES
(1, 'ajl', 'siswa', '$2y$10$0Xzi9HRGejGepgzGQMdSxe1or4j.sHZXLu1vrUkamczMw1ytoYPS.', 'siswa'),
(2, 'na', 'na', '$2y$10$TROm3ppp6KUIa8O0mPvDsuSlJeaUDwT0dQqGJz3rVwZPdPaLakJKS', 'siswa'),
(2005101, 'hen', 'hen', '$2y$10$lDFSkvwrAgbG0xDcvGZ2beBPEDdiGxsQSgN3YILufFl.7BDF6.CrC', '');

-- --------------------------------------------------------

--
-- Table structure for table `tugas`
--

CREATE TABLE `tugas` (
  `kode` varchar(8) NOT NULL,
  `pertemuan` varchar(8) NOT NULL,
  `judul` text NOT NULL,
  `deskripsi` text DEFAULT NULL,
  `deadline` datetime NOT NULL,
  `lampiran` text DEFAULT NULL,
  `mimetype` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Triggers `tugas`
--
DELIMITER $$
CREATE TRIGGER `del_related_submit_tugas` BEFORE DELETE ON `tugas` FOR EACH ROW BEGIN
    DELETE FROM Submit_Tugas WHERE tugas = OLD.kode;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `ujian`
--

CREATE TABLE `ujian` (
  `kode` varchar(8) NOT NULL,
  `pertemuan` varchar(8) NOT NULL,
  `durasi` time NOT NULL,
  `catatan` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Triggers `ujian`
--
DELIMITER $$
CREATE TRIGGER `del_related_jwb_ujian` BEFORE DELETE ON `ujian` FOR EACH ROW BEGIN
    DELETE FROM Jawaban_Ujian WHERE ujian = OLD.kode;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `del_related_nilai_ujian` BEFORE DELETE ON `ujian` FOR EACH ROW BEGIN
    DELETE FROM Nilai_Ujian WHERE ujian = OLD.kode;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `del_related_paket_soal` BEFORE DELETE ON `ujian` FOR EACH ROW BEGIN
    DELETE FROM Paket_Soal WHERE ujian = OLD.kode;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `simpan_nilai_ujian` AFTER INSERT ON `ujian` FOR EACH ROW BEGIN
    DECLARE finished BOOLEAN DEFAULT FALSE;
    DECLARE nim VARCHAR(7);
    DECLARE list_mhs CURSOR FOR
        SELECT kk.mahasiswa FROM Kontrak_Kelas AS kk
        INNER JOIN Pertemuan AS prt
            ON prt.kode = NEW.pertemuan
        WHERE kk.kelas = prt.kelas
    ;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET finished = TRUE;

    OPEN list_mhs;
    getMhs: LOOP
        FETCH list_mhs INTO nim;
        IF finished = TRUE THEN
            LEAVE getMhs;
        END IF;

        -- memasukkan data nilai ke tabel nilai
        INSERT INTO Nilai_Ujian (ujian, mahasiswa)
            VALUES (NEW.kode, nim)
        ;
    END LOOP getMhs;
    CLOSE list_mhs;

END
$$
DELIMITER ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dosen`
--
ALTER TABLE `dosen`
  ADD PRIMARY KEY (`kode`);

--
-- Indexes for table `jawaban_ujian`
--
ALTER TABLE `jawaban_ujian`
  ADD PRIMARY KEY (`kode`),
  ADD KEY `ujian` (`ujian`),
  ADD KEY `mahasiswa` (`mahasiswa`),
  ADD KEY `soal` (`soal`);

--
-- Indexes for table `kehadiran`
--
ALTER TABLE `kehadiran`
  ADD PRIMARY KEY (`kode`),
  ADD KEY `pertemuan` (`pertemuan`),
  ADD KEY `mahasiswa` (`mahasiswa`);

--
-- Indexes for table `kelas`
--
ALTER TABLE `kelas`
  ADD PRIMARY KEY (`kode`),
  ADD KEY `mata_kuliah` (`mata_kuliah`);

--
-- Indexes for table `kontrak_kelas`
--
ALTER TABLE `kontrak_kelas`
  ADD PRIMARY KEY (`kode`),
  ADD KEY `mahasiswa` (`mahasiswa`),
  ADD KEY `kelas` (`kelas`);

--
-- Indexes for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD PRIMARY KEY (`nim`);

--
-- Indexes for table `mata_kuliah`
--
ALTER TABLE `mata_kuliah`
  ADD PRIMARY KEY (`kode`),
  ADD KEY `dosen_pengampu1` (`dosen_pengampu1`),
  ADD KEY `dosen_pengampu2` (`dosen_pengampu2`);

--
-- Indexes for table `materi`
--
ALTER TABLE `materi`
  ADD PRIMARY KEY (`kode`),
  ADD KEY `pertemuan` (`pertemuan`);

--
-- Indexes for table `nilai_ujian`
--
ALTER TABLE `nilai_ujian`
  ADD PRIMARY KEY (`kode`),
  ADD KEY `ujian` (`ujian`),
  ADD KEY `mahasiswa` (`mahasiswa`);

--
-- Indexes for table `opsi_pg`
--
ALTER TABLE `opsi_pg`
  ADD PRIMARY KEY (`kode`),
  ADD KEY `soal` (`soal`);

--
-- Indexes for table `paket_soal`
--
ALTER TABLE `paket_soal`
  ADD PRIMARY KEY (`kode`),
  ADD KEY `ujian` (`ujian`),
  ADD KEY `soal` (`soal`);

--
-- Indexes for table `pertemuan`
--
ALTER TABLE `pertemuan`
  ADD PRIMARY KEY (`kode`),
  ADD KEY `kelas` (`kelas`);

--
-- Indexes for table `root`
--
ALTER TABLE `root`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rps`
--
ALTER TABLE `rps`
  ADD PRIMARY KEY (`kode`),
  ADD KEY `mata_kuliah` (`mata_kuliah`);

--
-- Indexes for table `silabus`
--
ALTER TABLE `silabus`
  ADD PRIMARY KEY (`kode`),
  ADD KEY `mata_kuliah` (`mata_kuliah`);

--
-- Indexes for table `soal`
--
ALTER TABLE `soal`
  ADD PRIMARY KEY (`kode`);

--
-- Indexes for table `submit_tugas`
--
ALTER TABLE `submit_tugas`
  ADD PRIMARY KEY (`kode`),
  ADD KEY `tugas` (`tugas`),
  ADD KEY `mahasiswa` (`mahasiswa`);

--
-- Indexes for table `tb_guru`
--
ALTER TABLE `tb_guru`
  ADD PRIMARY KEY (`id_guru`);

--
-- Indexes for table `tb_kelas`
--
ALTER TABLE `tb_kelas`
  ADD PRIMARY KEY (`kode_kelas`),
  ADD KEY `pengajar` (`pengajar`);

--
-- Indexes for table `tb_siswa`
--
ALTER TABLE `tb_siswa`
  ADD PRIMARY KEY (`id_siswa`);

--
-- Indexes for table `tugas`
--
ALTER TABLE `tugas`
  ADD PRIMARY KEY (`kode`),
  ADD KEY `pertemuan` (`pertemuan`);

--
-- Indexes for table `ujian`
--
ALTER TABLE `ujian`
  ADD PRIMARY KEY (`kode`),
  ADD KEY `pertemuan` (`pertemuan`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `kehadiran`
--
ALTER TABLE `kehadiran`
  MODIFY `kode` int(8) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nilai_ujian`
--
ALTER TABLE `nilai_ujian`
  MODIFY `kode` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `root`
--
ALTER TABLE `root`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `jawaban_ujian`
--
ALTER TABLE `jawaban_ujian`
  ADD CONSTRAINT `jawaban_ujian_ibfk_1` FOREIGN KEY (`ujian`) REFERENCES `ujian` (`kode`),
  ADD CONSTRAINT `jawaban_ujian_ibfk_2` FOREIGN KEY (`mahasiswa`) REFERENCES `mahasiswa` (`nim`),
  ADD CONSTRAINT `jawaban_ujian_ibfk_3` FOREIGN KEY (`soal`) REFERENCES `soal` (`kode`);

--
-- Constraints for table `kehadiran`
--
ALTER TABLE `kehadiran`
  ADD CONSTRAINT `kehadiran_ibfk_1` FOREIGN KEY (`pertemuan`) REFERENCES `pertemuan` (`kode`),
  ADD CONSTRAINT `kehadiran_ibfk_2` FOREIGN KEY (`mahasiswa`) REFERENCES `mahasiswa` (`nim`);

--
-- Constraints for table `kelas`
--
ALTER TABLE `kelas`
  ADD CONSTRAINT `kelas_ibfk_1` FOREIGN KEY (`mata_kuliah`) REFERENCES `mata_kuliah` (`kode`);

--
-- Constraints for table `kontrak_kelas`
--
ALTER TABLE `kontrak_kelas`
  ADD CONSTRAINT `kontrak_kelas_ibfk_1` FOREIGN KEY (`mahasiswa`) REFERENCES `mahasiswa` (`nim`),
  ADD CONSTRAINT `kontrak_kelas_ibfk_2` FOREIGN KEY (`kelas`) REFERENCES `kelas` (`kode`);

--
-- Constraints for table `mata_kuliah`
--
ALTER TABLE `mata_kuliah`
  ADD CONSTRAINT `mata_kuliah_ibfk_1` FOREIGN KEY (`dosen_pengampu1`) REFERENCES `dosen` (`kode`),
  ADD CONSTRAINT `mata_kuliah_ibfk_2` FOREIGN KEY (`dosen_pengampu2`) REFERENCES `dosen` (`kode`);

--
-- Constraints for table `materi`
--
ALTER TABLE `materi`
  ADD CONSTRAINT `materi_ibfk_1` FOREIGN KEY (`pertemuan`) REFERENCES `tb_kelas` (`kode_kelas`);

--
-- Constraints for table `nilai_ujian`
--
ALTER TABLE `nilai_ujian`
  ADD CONSTRAINT `nilai_ujian_ibfk_1` FOREIGN KEY (`ujian`) REFERENCES `ujian` (`kode`),
  ADD CONSTRAINT `nilai_ujian_ibfk_2` FOREIGN KEY (`mahasiswa`) REFERENCES `mahasiswa` (`nim`);

--
-- Constraints for table `opsi_pg`
--
ALTER TABLE `opsi_pg`
  ADD CONSTRAINT `opsi_pg_ibfk_1` FOREIGN KEY (`soal`) REFERENCES `soal` (`kode`);

--
-- Constraints for table `paket_soal`
--
ALTER TABLE `paket_soal`
  ADD CONSTRAINT `paket_soal_ibfk_1` FOREIGN KEY (`ujian`) REFERENCES `ujian` (`kode`),
  ADD CONSTRAINT `paket_soal_ibfk_2` FOREIGN KEY (`soal`) REFERENCES `soal` (`kode`);

--
-- Constraints for table `pertemuan`
--
ALTER TABLE `pertemuan`
  ADD CONSTRAINT `pertemuan_ibfk_1` FOREIGN KEY (`kelas`) REFERENCES `kelas` (`kode`);

--
-- Constraints for table `rps`
--
ALTER TABLE `rps`
  ADD CONSTRAINT `rps_ibfk_1` FOREIGN KEY (`mata_kuliah`) REFERENCES `mata_kuliah` (`kode`);

--
-- Constraints for table `silabus`
--
ALTER TABLE `silabus`
  ADD CONSTRAINT `silabus_ibfk_1` FOREIGN KEY (`mata_kuliah`) REFERENCES `mata_kuliah` (`kode`);

--
-- Constraints for table `submit_tugas`
--
ALTER TABLE `submit_tugas`
  ADD CONSTRAINT `submit_tugas_ibfk_1` FOREIGN KEY (`tugas`) REFERENCES `tugas` (`kode`),
  ADD CONSTRAINT `submit_tugas_ibfk_2` FOREIGN KEY (`mahasiswa`) REFERENCES `mahasiswa` (`nim`);

--
-- Constraints for table `tugas`
--
ALTER TABLE `tugas`
  ADD CONSTRAINT `tugas_ibfk_1` FOREIGN KEY (`pertemuan`) REFERENCES `pertemuan` (`kode`);

--
-- Constraints for table `ujian`
--
ALTER TABLE `ujian`
  ADD CONSTRAINT `ujian_ibfk_1` FOREIGN KEY (`pertemuan`) REFERENCES `pertemuan` (`kode`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
