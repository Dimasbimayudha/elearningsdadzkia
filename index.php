<?php
ob_start();
session_start();

// session_unset();
// session_destroy();
include './config/db.php';
if (isset($_SESSION["login"])) {
//   $username =  mysqli_real_escape_string($con, strtolower(stripslashes($_POST["username"])));
//   $password = mysqli_real_escape_string($con, $_POST["password"]);
//   $admin = mysqli_query($con, "SELECT * FROM admin WHERE username = '$username'");
  if ($_SESSION["level"] == "SU_ADMIN") {
    header("Location: guru/admin.php");
  } else {
    header("Location: siswa/siswa.php");
  }
  exit;
}

if (isset($_POST['login'])) {
  // session_unset();
  // session_destroy();
  // $_SESSION = [];
  global $koneksi;
  $username =  mysqli_real_escape_string($con, strtolower(stripslashes($_POST["username"])));
  $password = mysqli_real_escape_string($con, $_POST["password"]);

  $guru = mysqli_query($con, "SELECT * FROM tb_guru WHERE username = '$username'");
  $siswa = mysqli_query($con, "SELECT * FROM tb_siswa WHERE username = '$username'");

  if (mysqli_num_rows($guru) === 1) {
    $row = mysqli_fetch_assoc($guru); 
    if (password_verify($password, $row['password'])) {
      $_SESSION["login"] = true;
      $_SESSION['user'] = array(
        'id' => $row['id_guru'],
        'name' => $row['nm_guru']
    );
      $_SESSION["level"] = $row['level'];
      echo $_SESSION["level"];
      header("Location: projectKP/data_guru.php");
      exit();
    }
  }
  if (mysqli_num_rows($siswa) === 1) {
    $row = mysqli_fetch_assoc($siswa);
    if (password_verify($password, $row['password'])) {
      $_SESSION["login"] = true;
      $_SESSION['user'] = array(
        'id' => $row['id_siswa'],
        'name' => $row['nm_siswa']
      );
      $_SESSION["level"] = $row['level'];
      header("location : siswa/siswa.php");
      echo 'sip';
      exit();
    }
  }
  if (isset($_SESSION['level'])) {
    if ($_SESSION['level'] == 'guru') {
      echo 'seesiontrue';
      var_dump($_SESSION);
      header("Location: guru/admin.php");
      exit();
    } else {
      header("Location: siswa/siswa.php");
      exit();
    }
  }


  var_dump($_SESSION);
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="syle/style.css">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
  <title>Document</title>
</head>

<body>
  <!-- Tampilan Header
  <header class="text-bg-primary p-3 d-flex flex-wrap align-items-center justify-content-center justify-content-md-between py-3 mb-4 border-bottom">
    <a href="index.php" class="d-flex align-items-center col-md-3 mb-2 mb-md-0 text-dark text-decoration-none">
      <svg class="bi me-2" width="40" height="32" role="img" src aria-label="Bootstrap"> <img src="https://1.bp.blogspot.com/-NW9KFQ_0Urg/XpPRfyBUHzI/AAAAAAAADVM/BS9XQmCdKTohgunfffxNawU3EcHOquBBgCLcBGAsYHQ/s1600/LOGO%2Btutwurihandayani.jpg" alt="" width="40" height="40"></svg>
    </a> -->

    <!-- <ul class="nav col-12 col-md-auto mb-2 justify-content-center mb-md-0"> -->
      <!-- <li><a href="index.php" class="nav-link px-2 link-light">Elearning SDN Sirapan 01 Madiun</a></li> -->
    </ul>

    <!-- <div class="col-md-3 text-end"> -->
      <!-- <a href="login.php" class="btn btn-light">Sign-in</a> -->
    </div>
  </header>
  <a href="./projectKP/admin.php"></a>
  <div id="layoutAuthentication">
    <div id="layoutAuthentication_content">
      <main>
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-lg-5">
              <div class="card shadow-lg border-0 rounded-lg mt-5">
                <div class="card-header">
                  <h3 class="text-center font-weight-light my-4">Login Guru</h3>
                </div>
                <div class="card-body">
                  <form method='POST' action="">
                    <div class="form-floating mb-3">
                      <input class="form-control" id="Username" type="text" placeholder="name@example.com" name='username' required />
                      <label for="inputEmail">Username</label>
                    </div>
                    <div class="form-floating mb-3">
                      <input class="form-control" id="inputPassword" type="password" placeholder="Password" name='password' required />
                      <label for="inputPassword">Password</label>
                    </div>
                    <div class="form-check mb-3">
                      <input class="form-check-input" id="inputRememberPassword" type="checkbox" value="" />
                      <label class="form-check-label" for="inputRememberPassword">Remember Password</label>
                    </div>
                    <div class="d-flex align-items-center justify-content-between mt-4 mb-0">
                      <a class="small" href="password.html">Forgot Password?</a>
                      <button class="btn btn-primary" name='login'>Login</button>
                    </div>
                  </form>
                </div>
                <div class="card-footer text-center py-3">
                  <div class="text-muted">Copyright &copy; SD IT ADZKIA <?= date('Y') ?></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
    </div>


  </div>
  <!-- End of .container -->
  </div>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
</body>

</html>